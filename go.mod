module bitbucket.org/artyom_p/offsiter

require (
	github.com/aws/aws-sdk-go v1.16.31
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/fs v0.1.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pkg/sftp v1.10.0
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20190208162236-193df9c0f06f
	golang.org/x/net v0.0.0-20190206173232-65e2d4e15006 // indirect
	golang.org/x/sys v0.0.0-20190209173611-3b5209105503 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
