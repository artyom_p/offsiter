package offsiter

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// UploadResult holds name and checksum of successfully uploaded file
type UploadResult struct {
	Name string
	Hash []byte // sha1.Sum
}

func CollectResults(conf *Config, res chan UploadResult, done chan struct{}) {
	defer close(done)
	cnt := 0
	f, err := ioutil.TempFile(os.TempDir(), "")
	if err != nil {
		log.Print(err)
		for _ = range res {
			// drain channel until it's closed
		}
		return
	}
	defer f.Close()
	if err := os.Remove(f.Name()); err != nil {
		log.Print(err)
	}
	ew := &errWriter{w: f}
	for r := range res {
		cnt++
		fmt.Fprintf(ew, "%x\t%s\n", r.Hash, r.Name)
	}
	// upload results to S3 if not empty
	if cnt == 0 {
		return
	}
	if ew.err != nil {
		log.Print(ew.err)
		return // file incomplete, won't upload, just return
	}
	// seek to begin and upload content
	if _, err := f.Seek(0, os.SEEK_SET); err != nil {
		log.Print(err)
		return
	}
	_, err = conf.uploader.Upload(&s3manager.UploadInput{
		Bucket:      &conf.S3Bucket,
		Key:         aws.String(path.Join(conf.S3Prefix, "sha1sums")),
		Body:        f,
		ContentType: aws.String("text/plain"),
		ACL:         aws.String("private"),
	})
	if err != nil {
		log.Print(err)
	}
}

type errWriter struct {
	w   io.Writer
	err error
}

func (ew *errWriter) Write(buf []byte) (int, error) {
	if ew.err != nil {
		return len(buf), nil
	}
	_, ew.err = ew.w.Write(buf)
	return len(buf), nil
}
