package offsiter

import (
	"errors"
	"log"
	"net"
	"path"
	"sync"
	"time"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

func DoRemotes(conf *Config, res chan UploadResult, wg *sync.WaitGroup) {
	defer wg.Done()
	for _, rs := range conf.Remotes {
		conf.fetchMutex.Lock()
		wg.Add(1)
		go fetchAndUpload(conf, rs, res, wg)
	}
}

func fetchAndUpload(conf *Config, rs RemoteSpec, res chan UploadResult, wg *sync.WaitGroup) {
	defer wg.Done()
	defer conf.fetchMutex.Unlock()
	sshConfig := &ssh.ClientConfig{
		User:            rs.User,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(rs.signer), ssh.KeyboardInteractive(keyboardChallenge)},
		Timeout:         time.Minute,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	dialer := net.Dialer{Timeout: time.Minute, KeepAlive: 3 * time.Minute}
	tcpConn, err := dialer.Dial("tcp", rs.Address)
	if err != nil {
		log.Printf("error while connecting to %q: %v", rs.Address, err)
		return
	}
	defer tcpConn.Close()
	sshConn, _chans, _reqs, err := ssh.NewClientConn(tcpConn, rs.Address, sshConfig)
	if err != nil {
		log.Printf("error on ssh handshake to %q: %v", rs.Address, err)
		return
	}
	sshClient := ssh.NewClient(sshConn, _chans, _reqs)
	defer sshClient.Close()
	sftpClient, err := sftp.NewClient(sshClient)
	if err != nil {
		log.Printf("error while establishing sftp session to %q: %v", rs.Address, err)
		return
	}
	defer sftpClient.Close()
	entries, err := sftpClient.ReadDir(rs.Directory)
	if err != nil {
		log.Print(err)
		return
	}
	for _, fi := range entries {
		if fi.IsDir() {
			continue
		}
		if rs.Match != "" {
			switch ok, err := path.Match(rs.Match, fi.Name()); {
			case err != nil:
				log.Print(err)
				return
			case !ok:
				continue
			}
		}
		rFile, err := sftpClient.Open(path.Join(rs.Directory, fi.Name()))
		if err != nil {
			log.Print(err)
			continue
		}
		name := path.Join(conf.S3Prefix, rs.S3Prefix, fi.Name())
		conf.uploadMutex.Lock()
		h, err := PutEncrypted(conf.uploader, conf.recipients, conf.signer, rFile, conf.S3Bucket, name)
		rFile.Close()
		conf.uploadMutex.Unlock()
		if err != nil {
			log.Print(err)
			continue
		}
		res <- UploadResult{
			Name: path.Join(rs.S3Prefix, fi.Name()),
			Hash: h.Sum(nil),
		}
	}
}

func keyboardChallenge(user, instruction string, questions []string, echos []bool) (answers []string, err error) {
	if len(questions) == 0 {
		return nil, nil
	}
	return nil, errors.New("keyboard interactive challenge is not supported")
}
