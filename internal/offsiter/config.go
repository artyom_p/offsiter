package offsiter

import (
	"errors"
	"io/ioutil"
	"os"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
	"golang.org/x/crypto/ssh"
)

type Config struct {
	S3Bucket           string
	S3AccessKey        string
	S3SecretKey        string
	S3Region           string
	S3Prefix           string
	SigningKey         string   // path to private ascii-armored PGP key used to sign
	RecipientKeys      []string // paths to separate public ascii-armored PGP keys
	UploadConcurrency  int
	CommandConcurrency int
	RemoteConcurrency  int
	Commands           []CmdSpec
	Remotes            []RemoteSpec

	// items below are filled by calling Config.Validate
	uploader *s3manager.Uploader

	signer     *openpgp.Entity
	recipients []*openpgp.Entity

	runMutex    sync.Locker
	fetchMutex  sync.Locker
	uploadMutex sync.Locker
}

type CmdSpec struct {
	Command  string // command is run via sh
	S3Prefix string
}

type RemoteSpec struct {
	Address   string // host:port
	User      string
	SSHKey    string // path to RSA private key in PEM format
	Directory string
	Match     string // mask used by path.Match
	S3Prefix  string

	// items below are filled by calling Config.Validate
	signer ssh.Signer
}

func (c *Config) Validate() error {
	p := "invalid config: "
	if len(c.Commands) == 0 && len(c.Remotes) == 0 {
		return errors.New(p + "either commands or remotes should be non-empty")
	}
	creds := credentials.NewStaticCredentials(c.S3AccessKey, c.S3SecretKey, "")
	awsconf := aws.NewConfig().WithRegion(c.S3Region).WithCredentials(creds).WithMaxRetries(10)
	c.uploader = s3manager.NewUploader(session.New(awsconf))
	// ref: Mon Jan 2 15:04:05 -0700 MST 2006
	dt := time.Now().Format("2006-01-02")
	switch c.S3Prefix {
	case "":
		c.S3Prefix = dt
	default:
		c.S3Prefix = c.S3Prefix + "-" + dt
	}
	if c.SigningKey != "" {
		var err error
		if c.signer, err = readKey(c.SigningKey); err != nil {
			return err
		}
	}
	if len(c.RecipientKeys) == 0 {
		return errors.New("no recipient PGP keys")
	}
	for _, name := range c.RecipientKeys {
		switch ent, err := readKey(name); err {
		case nil:
			c.recipients = append(c.recipients, ent)
		default:
			return err
		}
	}
	m := make(map[string]ssh.Signer)
	for i, rs := range c.Remotes {
		// don't parse the same key twice
		if s, ok := m[rs.SSHKey]; ok {
			c.Remotes[i].signer = s
			continue
		}
		b, err := ioutil.ReadFile(rs.SSHKey)
		if err != nil {
			return err
		}
		signer, err := ssh.ParsePrivateKey(b)
		if err != nil {
			return err
		}
		m[rs.SSHKey] = signer
		c.Remotes[i].signer = signer
	}
	c.uploadMutex = newGate(c.UploadConcurrency)
	c.runMutex = newGate(c.CommandConcurrency)
	c.fetchMutex = newGate(c.RemoteConcurrency)
	return nil
}

func readKey(name string) (*openpgp.Entity, error) {
	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	block, err := armor.Decode(f)
	if err != nil {
		return nil, err
	}
	return openpgp.ReadEntity(packet.NewReader(block.Body))
}

// gate implements sync.Locker with one extension: gate has capacity and can be
// locked up to its capacity before new calls to Lock() would block
type gate chan struct{}

func newGate(n int) gate {
	if n < 1 {
		n = 1
	}
	return make(chan struct{}, n)
}

func (g gate) Lock() { g <- struct{}{} }

func (g gate) Unlock() { <-g }
