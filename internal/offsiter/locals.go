package offsiter

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"sync"

	"github.com/kr/fs"
)

func DoCommands(conf *Config, res chan UploadResult, wg *sync.WaitGroup) {
	defer wg.Done()
	for _, cs := range conf.Commands {
		conf.runMutex.Lock()
		wg.Add(1)
		go runAndUpload(conf, cs, res, wg)
	}
}

func runAndUpload(conf *Config, cs CmdSpec, res chan UploadResult, wg *sync.WaitGroup) {
	defer wg.Done()
	tdir, err := ioutil.TempDir("", "cmd_")
	if err != nil {
		log.Print(err)
		return
	}
	var (
		removeOut bool
		outFile   *os.File
	)
	defer func() {
		if err := os.RemoveAll(tdir); err != nil {
			log.Print(err)
		}
		if removeOut {
			os.Remove(outFile.Name())
		}
	}()
	cmd := exec.Command("sh", "-c", cs.Command)
	cmd.Dir = tdir
	if outFile, err = ioutil.TempFile("", "cmd-log-"); err == nil {
		cmd.Stdout = outFile
		cmd.Stderr = outFile
		defer outFile.Close()
	}
	err = cmd.Run()
	conf.runMutex.Unlock() // release early, so others can run too
	if err != nil {
		if outFile != nil {
			log.Printf("command %q error (see %q): %v",
				cs.Command, outFile.Name(), err)
		} else {
			log.Printf("command %q error: %v", cs.Command, err)
		}
		return
	}
	removeOut = true
	walker := fs.Walk(tdir)
	for walker.Step() {
		if err := walker.Err(); err != nil {
			log.Print(err)
			continue
		}
		fi := walker.Stat()
		if fi.IsDir() {
			continue
		}
		lFile, err := os.Open(walker.Path())
		if err != nil {
			log.Print(err)
			continue
		}
		name := path.Join(
			conf.S3Prefix,
			cs.S3Prefix,
			strings.TrimPrefix(walker.Path(), tdir),
		)
		conf.uploadMutex.Lock()
		h, err := PutEncrypted(conf.uploader, conf.recipients, conf.signer, lFile, conf.S3Bucket, name)
		lFile.Close()
		conf.uploadMutex.Unlock()
		if err != nil {
			log.Print(err)
			continue
		}
		os.Remove(walker.Path()) // remove file early to save space for parallel threads
		res <- UploadResult{
			Name: path.Join(cs.S3Prefix, strings.TrimPrefix(walker.Path(), tdir)),
			Hash: h.Sum(nil),
		}
	}
}
