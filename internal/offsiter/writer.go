package offsiter

import (
	"crypto/sha1"
	"hash"
	"io"

	"golang.org/x/crypto/openpgp"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// PutEncrypted encrypts and uploads given reader to S3 bucket
func PutEncrypted(uploader *s3manager.Uploader, recip []*openpgp.Entity, signer *openpgp.Entity, r io.Reader, bkt, p string) (hash.Hash, error) {
	pipeR, pipeW := io.Pipe()
	errCh := make(chan error, 1)
	go func() {
		_, err := uploader.Upload(&s3manager.UploadInput{
			Bucket:      &bkt,
			Key:         &p,
			Body:        pipeR,
			ContentType: aws.String("application/octet-stream"),
			ACL:         aws.String("private"),
		})
		switch err {
		case nil:
			pipeR.Close()
		default:
			// need to close pipeR: if PutReader fails and stops
			// reading from pipeR and it is left open, writing to
			// pipeW will block forever
			pipeR.CloseWithError(err)
		}
		errCh <- err
	}()
	h, err := Encrypt(recip, signer, pipeW, r)
	switch err {
	case nil:
		// we need to close write pipe end so that reads from reader
		// pipe end would return with io.EOF error, which PutReader
		// treats as normal stream completion
		pipeW.Close()
	default:
		pipeW.CloseWithError(err)
	}
	if err1 := <-errCh; err1 != nil {
		return nil, err1
	}
	if err != nil {
		return nil, err
	}
	return h, nil
}

// Encrypt reads data from reader, encrypts them and writes to writer, returning
// hash of data written and error.
func Encrypt(recip []*openpgp.Entity, signer *openpgp.Entity, w io.Writer, r io.Reader) (hash.Hash, error) {
	h := sha1.New()
	mw := io.MultiWriter(h, w)
	wc, err := openpgp.Encrypt(mw, recip, signer, &openpgp.FileHints{IsBinary: true}, nil)
	if err != nil {
		return nil, err
	}
	if _, err := io.Copy(wc, r); err != nil {
		return nil, err
	}
	return h, wc.Close()
}
