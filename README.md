`offsiter` is a tool to create offsite backups to Amazon S3.

Features:

* produce local backups with arbitrary commands
* fetch files from remote hosts via sftp
* upload files to S3 with on-the-fly encryption

`offsiter` can run arbitrary commands which are expected to produce some files
to backup. Commands are run in temporary directory which is removed after all
produced files were processed and are expected to dump files to their current
directory (`$PWD`). Please check if you have sufficient disk space in your
`$TMPDIR`.

`offsiter` can also connect to remote hosts via ssh protocol using given ssh
private key (which should not be password-protected) and directly read remote
files via sftp, uncrypting and uploading data stream without storing local
copy.

All uploaded files are encrypted to a number of public PGP keys provided with
`recipientkeys` option, optionally signed with private PGP key specified with
`signingkey`. To decrypt use regular `gpg(1)` tool like this:

	gpg --decrypt --output output-file.bin encrypted.bin

Example configuration file (yaml format):

```yaml
s3bucket: my-bucket
s3prefix: backup
s3region: us-west-1
s3accesskey: AWS_ACCESS_KEY
s3secretkey: AWS_SECRET_KEY

signingkey: /path/to/pgp-private-key.txt
recipientkeys:
 - /path/to/pgp-public-key1.txt
 - /path/to/pgp-public-key2.txt

uploadconcurrency: 10
remoteconcurrency: 5
commandconcurrency: 1

commands:
  - s3prefix: databases
    command: mysqldump --all-databases --host db.host | gzip > dump.sql.gz
  - s3prefix: sites
    command: tar czf htdocs.tar.gz /var/www

remotes:
  - address: server1.mydomain:22
    s3prefix: server1_data
    user: bot
    sshkey: /path/to/ssh-private-key.id_rsa
    directory: /var/backup
    match: "*.tar.gz"
  - address: server2.mydomain:22
    s3prefix: server2_data
    user: bot
    sshkey: /path/to/ssh-private-key.id_rsa
    directory: /var/lib/uploads
    match: "*"
```

Top-level `s3prefix` is appended with current date in `-YYYY-MM-DD` format, so
with config above final layout in S3 bucket may look like this:

	backup-2015-01-16/databases/dump.sql.gz
	backup-2015-01-16/sites/htdocs.tar.gz
	backup-2015-01-16/server1_data/....tar.gz
	...
	backup-2015-01-16/server2_data/...
	backup-2015-01-16/sha1sums

File `sha1sums` is uploaded as the last one and holds sha1 checksums for every
file uploaded (checksums calculated over encrypted stream, so downloaded data
can be verified using this checksums).

If any `s3prefix` is omitted or empty, it won't be added, if top-level
`s3prefix` is empty, it will be replaced by date in `YYYY-MM-DD` format.

Options that limits number of concurrent threads:

* `uploadconcurrency` limits number of simultaneous uploads to S3;
* `remoteconcurrency` limits number of simultaneous remote ssh conneections;
* `commandconcurrency` limits number of external commands that can
  simultaneously run.

Commands specified with `command` options are run via `sh` shell, so you can
use redirection, etc. Commands are started within empty per-command temporary
directory and are expected to produce output in it.

For remote connections, only files are copied, directories are neither copied,
nor descended into. If `match` option is missing, it works as star-glob (match
all). Since files are transferred to S3 without copying, they are expected to
be in atomic state: nobody has to write/append them, otherwise they would be
uploaded in inconsistent state.

All errors are written to stderr, upload/read/run command errors are not
considered fatal and `offsiter` can exit with 0 code even if some files were
not uploaded. As all errors are logged to stderr, you should examine output to
see if anything went wrong . This may change in the future.
