package main

import (
	"flag"

	"io/ioutil"
	"log"
	"os"
	"sync"

	"bitbucket.org/artyom_p/offsiter/internal/offsiter"
	"gopkg.in/yaml.v2"
)

func init() { log.SetFlags(log.Lshortfile) }

func main() {
	confFile := flag.String("config", "", "path to config in yaml format")
	flag.Parse()
	if *confFile == "" {
		flag.Usage()
		os.Exit(1)
	}
	config, err := readConfig(*confFile)
	if err != nil {
		log.Fatal(err)
	}
	if err := config.Validate(); err != nil {
		log.Fatal(err)
	}
	// collect UploadResult(s) to file, if any of the files were uploaded
	// successfully, data from UploadResult file would be uploaded to S3.
	// All errors would go to stderr
	done := make(chan struct{}) // close would notify we're done
	results := make(chan offsiter.UploadResult)

	go offsiter.CollectResults(config, results, done)

	wg := new(sync.WaitGroup)
	// spawn background goroutines, all of them are guarded by
	// wg.Add/wg.Done. When all finish, wg.Wait() unblocks so we can close
	// results channel to unblock receive loop of CollectResults
	wg.Add(2)
	go offsiter.DoCommands(config, results, wg)
	go offsiter.DoRemotes(config, results, wg)
	wg.Wait()
	close(results)
	<-done // CollectResults would close it
}

// readConfig reads and parses given configuration file
func readConfig(path string) (*offsiter.Config, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	conf := new(offsiter.Config)
	if err := yaml.Unmarshal(b, conf); err != nil {
		return nil, err
	}
	return conf, nil
}
